#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <math.h>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float32.h"
#include "voltron_msgs/msg/vcu_control_msg.hpp"
#include <ackermann_msgs/msg/ackermann_drive_stamped.hpp>

// constants
const float WHEEL_CIRCUMFERENCE = 0.91765921411f;
const float DRIVE_RATIO = 3.0f;

using namespace std::chrono_literals;

class ackermationn_translator : public rclcpp::Node
{
  private:
    // member vars
    std::string SUB_TOPIC_;
    std::string PUB_TOPIC_;
    float steeringAngle_out_;
    float steeringAngle_in_;
    float driveSpeed_in_;
    float driveSpeed_out_;
    rclcpp::Publisher<voltron_msgs::msg::VcuControlMsg>::SharedPtr publisher_; 
    rclcpp::Subscription<ackermann_msgs::msg::AckermannDriveStamped>::SharedPtr subscription_;
    size_t count_;

    // Every time we hear a new drive message
    void topic_callback(const ackermann_msgs::msg::AckermannDriveStamped msg) {
      // Ingest desired control commands
      steeringAngle_in_ = msg.drive.steering_angle;
      driveSpeed_in_ = msg.drive.speed;
      // Convert to VCU-friendly units
      steeringAngle_out_ = radians_to_motorRotations(steeringAngle_in_);
      driveSpeed_out_ = metersPerSecond_to_RPM(driveSpeed_in_);

      RCLCPP_INFO(this->get_logger(), "steering: '%f'", steeringAngle_in_);
      RCLCPP_INFO(this->get_logger(), "speed: '%f'", driveSpeed_in_);

      // publish
      auto msg_out = voltron_msgs::msg::VcuControlMsg();
      msg_out.steering_revs = steeringAngle_out_;
      msg_out.drive_rpm = driveSpeed_out_;
      publisher_->publish(msg_out);
    }

    inline float radians_to_motorRotations(float radians) {
      return (asin(radians / 1.42) / .105);
    }

    inline float metersPerSecond_to_RPM(float meters_per_second) {
      return ((meters_per_second / WHEEL_CIRCUMFERENCE) * 60) * DRIVE_RATIO;
    }

  public:
    ackermationn_translator() : Node("ackermationn_translator"), count_(0) {
      // setup params
      this->declare_parameter("PUB_TOPIC_PARAM", "/voltron_vcu/control_in");
      this->declare_parameter("SUB_TOPIC_PARAM", "/test_in");
      PUB_TOPIC_ = this->get_parameter("PUB_TOPIC_PARAM").get_parameter_value().get<std::string>();
      SUB_TOPIC_ = this->get_parameter("SUB_TOPIC_PARAM").get_parameter_value().get<std::string>();

      // setup pubsub
      publisher_ = this->create_publisher<voltron_msgs::msg::VcuControlMsg>(PUB_TOPIC_, 10);
      subscription_ = this->create_subscription<ackermann_msgs::msg::AckermannDriveStamped>(SUB_TOPIC_,
        10, std::bind(&ackermationn_translator::topic_callback, this, std::placeholders::_1));
    }
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ackermationn_translator>());
  rclcpp::shutdown();
  return 0;
}
