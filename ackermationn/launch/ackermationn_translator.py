from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package="ackermationn",
            executable="ackermationn_translator",
            name="ackermationn_translator",
            output="screen",
            emulate_tty=True,
            parameters=[
                {"PUB_TOPIC_PARAM": "/vcu_control"},
                {"SUB_TOPIC_PARAM": "/testingo"}
            ]
        )
    ])