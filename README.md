# ackermationn

---

`ackermationn`

Takes:
MPC Node Output = {desired steeringAngle inRadians, desired driveSpeed inMetersPerSecond}

And translates it to:
VCU Node Input = {commanded steeringAngle inOdriveRotations, commanded driveSpeed inVescRPM}